﻿using System;
using System.IO;
using System.Diagnostics;

namespace nazwy
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                if (File.Exists("changed.txt"))
                {
                    File.Delete("changed.txt");
                }
                if (File.Exists("java.txt"))
                {
                    File.Delete("java.txt");
                }
                if (File.Exists("txt.txt"))
                {
                    File.Delete("txt.txt");
                }
                if (File.Exists("xaml.txt"))
                {
                    File.Delete("xaml.txt");
                }
            }//Removing old files

            //Gets number
            Console.WriteLine("Generator kodu Java + XML dla Soundboardu z Kononowiczem" + Environment.NewLine + Environment.NewLine + "Liczba początkowa:");
            string input = Console.ReadLine();
            int from;
            Int32.TryParse(input, out from);

            DirectoryInfo d = new DirectoryInfo(@"Edit");
            FileInfo[] infos = d.GetFiles();
            foreach (FileInfo f in infos)
            {
                //Replacing spaces with underline
                string name = f.FullName;
                string ch = name.Replace(" ", "_");
                //Replacing polish characters with standard
                ch = ch.Replace("ą", "a");
                ch = ch.Replace("ś", "s");
                ch = ch.Replace("ć", "c");
                ch = ch.Replace("ż", "z");
                ch = ch.Replace("ź", "z");
                ch = ch.Replace("ę", "e");
                ch = ch.Replace("ó", "o");
                ch = ch.Replace("ł", "l");
                ch = ch.Replace("ń", "n");
                //Replacing all upper case with lower case characters
                ch = ch.ToLower();
                //Removing comma
                ch = ch.Replace(",", "");
                //Removing dash
                ch = ch.Replace("-", "");

                File.Move(name, ch);
                Console.WriteLine(name + " " + ch);
                File.AppendAllText("txt.txt", f.Name.Substring(0, f.Name.Length-4) + Environment.NewLine);
                string DirNum = Directory.GetCurrentDirectory();
                int NumToDel = DirNum.Length + 6;
                string chan1 = ch.Remove(0, NumToDel);
                string chan2 = chan1.Substring(0, chan1.Length - 4);
                File.AppendAllText("changed.txt", chan2 + Environment.NewLine);


                string l = Environment.NewLine;
                File.AppendAllText("xaml.txt",
                    "<Button" + l +
                    "android:id=\"@+id/button" + from + "\"" + l +
                    "android:layout_width=\"wrap_content\"" + l +
                    "android:layout_height=\"wrap_content\"" + l +
                    "android:layout_alignParentLeft=\"true\"" + l +
                    "android:layout_alignParentStart=\"true\"" + l +
                    "android:layout_below=\"@+id/button" + (from - 1) + "\"" + l +
                    "android:onClick=\"clickHandler\"" + l +
                    "android:text=\"" + f.Name.Substring(0, f.Name.Length - 4) + "\"" + " />" + l + l);

                File.AppendAllText("java.txt",
                    "case R.id.button" + from + ":" + l +
                    "mp = MediaPlayer.create(this, R.raw." + chan2 + ");" + l +
                    "mp.start();" + l +
                    "mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {" + l +
                    "@Override" + l +
                    "public void onCompletion(MediaPlayer mp) {" + l +
                    "    mp.release();" + l +
                    "}" + l +
                    "});" + l +
                    "break;" + l + l
                    );


                from++;
            }

            Process.Start("java.txt");
            Process.Start("xaml.txt");
        }
    }
}
